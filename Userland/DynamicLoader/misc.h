/*
 * Copyright (c) 2020, the SegueBaseOS developers.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

extern "C" {
const char* __cxa_demangle(const char*, void*, void*, int*);

extern void* __dso_handle;
}
