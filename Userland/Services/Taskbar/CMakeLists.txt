seguebase_component(
    Taskbar
    REQUIRED
    TARGETS Taskbar
)

set(SOURCES
    main.cpp
    ClockWidget.cpp
    ShutdownDialog.cpp
    TaskbarButton.cpp
    TaskbarWindow.cpp
    WindowList.cpp
)

seguebase_bin(Taskbar)
target_link_libraries(Taskbar LibGUI LibDesktop)
seguebase_install_headers(Services/Taskbar)
