/*
 * Copyright (c) 2021, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace SymbolServer {

class ClientConnection;

}
