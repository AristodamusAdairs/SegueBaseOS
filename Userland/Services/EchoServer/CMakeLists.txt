seguebase_component(
    EchoServer
    TARGETS EchoServer
)

set(SOURCES
    Client.cpp
    main.cpp
)

seguebase_bin(EchoServer)
target_link_libraries(EchoServer LibCore)
