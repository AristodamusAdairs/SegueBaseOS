/*
 * Copyright (c) 2020, the SegueBaseOS developers.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <unistd.h>

int main()
{
    sysbeep();
    return 0;
}
