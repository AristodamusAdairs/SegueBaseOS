/*
 * Copyright (c) 2018-2020, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <LibCore/NetworkResponse.h>

namespace Core {

NetworkResponse::NetworkResponse()
{
}

NetworkResponse::~NetworkResponse()
{
}

}
