/*
 * Copyright (c) 2020, the SegueBaseOS developers.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace Gemini {

class Document;
class GeminiRequest;
class GeminiResponse;
class GeminiJob;
class Job;

}
