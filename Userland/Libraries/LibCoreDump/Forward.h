/*
 * Copyright (c) 2020, Linus Groh <linusg@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace CoreDump {

class Backtrace;
class Reader;

}
