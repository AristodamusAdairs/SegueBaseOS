/*
 * Copyright (c) 2018-2020, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <LibWeb/HTML/HTMLHRElement.h>

namespace Web::HTML {

HTMLHRElement::HTMLHRElement(DOM::Document& document, QualifiedName qualified_name)
    : HTMLElement(document, move(qualified_name))
{
}

HTMLHRElement::~HTMLHRElement()
{
}

}
