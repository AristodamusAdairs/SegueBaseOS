/*
 * Copyright (c) 2020, Luke Wilde <lukew@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <LibWeb/DOM/DocumentFragment.h>

namespace Web::DOM {

DocumentFragment::DocumentFragment(Document& document)
    : ParentNode(document, NodeType::DOCUMENT_FRAGMENT_NODE)
{
}

DocumentFragment::~DocumentFragment()
{
}

}
