/*
 * Copyright (c) 2021, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <LibJS/Forward.h>

namespace Web::Bindings {

JS::VM& main_thread_vm();

}
