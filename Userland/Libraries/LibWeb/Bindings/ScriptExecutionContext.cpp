/*
 * Copyright (c) 2020, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <LibWeb/Bindings/ScriptExecutionContext.h>

namespace Web::Bindings {

ScriptExecutionContext::~ScriptExecutionContext()
{
}

}
