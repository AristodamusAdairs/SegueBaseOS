/*
 * Copyright (c) 2021, Linus Groh <linusg@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <AK/Forward.h>

namespace GUI {

String format_gml(const StringView&);

}
