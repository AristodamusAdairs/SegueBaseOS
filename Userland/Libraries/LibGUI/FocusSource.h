/*
 * Copyright (c) 2020, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace GUI {

enum class FocusSource {
    Programmatic,
    Keyboard,
    Mouse,
};

}
