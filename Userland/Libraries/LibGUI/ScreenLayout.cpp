/*
 * Copyright (c) 2021, the SegueBaseOS developers.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <Services/WindowServer/ScreenLayout.ipp>
