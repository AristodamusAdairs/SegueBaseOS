/*
 * Copyright (c) 2020, the SegueBaseOS developers.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace HTTP {

class HttpRequest;
class HttpResponse;
class HttpJob;
class HttpsJob;
class Job;

}
