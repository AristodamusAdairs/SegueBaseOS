/*
 * Copyright (c) 2021, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace Syntax {

class Highlighter;
class HighlighterClient;

}
