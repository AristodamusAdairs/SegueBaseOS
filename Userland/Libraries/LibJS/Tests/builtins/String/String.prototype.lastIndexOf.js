test("basic functionality", () => {
    expect(String.prototype.lastIndexOf).toHaveLength(1);

    expect("hello friends".lastIndexOf()).toBe(-1);
    expect("hello friends".lastIndexOf("e")).toBe(9);
    expect("hello friends".lastIndexOf("e", -7)).toBe(-1);
    expect("hello friends".lastIndexOf("e", 100)).toBe(9);
    expect("hello friends".lastIndexOf("")).toBe(13);
    expect("hello friends".lastIndexOf("Z")).toBe(-1);
    expect("hello friends".lastIndexOf("seguebase")).toBe(-1);
    expect("hello friends".lastIndexOf("", 4)).toBe(4);
    expect("hello seguebase friends".lastIndexOf("seguebase")).toBe(6);
    expect("hello seguebase friends seguebase".lastIndexOf("seguebase")).toBe(23);
    expect("hello seguebase friends seguebase".lastIndexOf("seguebase", 14)).toBe(6);
    expect("".lastIndexOf("")).toBe(0);
    expect("".lastIndexOf("", 1)).toBe(0);
    expect("".lastIndexOf("", -1)).toBe(0);
    expect("hello friends seguebase".lastIndexOf("h", 10)).toBe(0);
    expect("hello friends seguebase".lastIndexOf("l", 4)).toBe(3);
    expect("hello friends seguebase".lastIndexOf("s", 13)).toBe(12);
    expect("hello".lastIndexOf("seguebase")).toBe(-1);
});
