/*
 * Copyright (c) 2018-2020, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace Gfx {

enum class TextElision {
    None,
    Right,
};

}
