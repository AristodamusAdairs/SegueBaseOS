/*
 * Copyright (c) 2020, Andreas Kling <kling@seguebaseos.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <LibGfx/ClassicWindowTheme.h>
#include <LibGfx/WindowTheme.h>

namespace Gfx {

WindowTheme& WindowTheme::current()
{
    static ClassicWindowTheme theme;
    return theme;
}

WindowTheme::~WindowTheme()
{
}

}
