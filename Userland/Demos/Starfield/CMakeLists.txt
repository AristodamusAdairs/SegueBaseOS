seguebase_component(
    Starfield
    TARGETS Starfield
)

set(SOURCES
    Starfield.cpp
)

seguebase_app(Starfield ICON app-screensaver)
target_link_libraries(Starfield LibGUI LibCore LibGfx)
