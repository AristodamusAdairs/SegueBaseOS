/*
 * Copyright (c) 2020, the SegueBaseOS developers.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace Kernel {

enum class PageFaultResponse {
    ShouldCrash,
    OutOfMemory,
    Continue,
};

}
