#!/usr/bin/env bash

SEGUEBASE_PORTS_DIR="${SEGUEBASE_SOURCE_DIR}/Build/${SEGUEBASE_ARCH}/Root/usr/Ports"

for file in $(git ls-files "${SEGUEBASE_SOURCE_DIR}/Ports"); do
    if [ "$(basename "$file")" != ".hosted_defs.sh" ]; then
        target=${SEGUEBASE_PORTS_DIR}/$(realpath --relative-to="${SEGUEBASE_SOURCE_DIR}/Ports" "$file")
        mkdir -p "$(dirname "$target")" && cp "$file" "$target"
    fi
done
