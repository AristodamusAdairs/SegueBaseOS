#!/bin/sh
# shellcheck disable=SC2086 # FIXME: fix these globing warnings

set -e

die() {
    echo "die: $*"
    exit 1
}

#SEGUEBASE_PACKET_LOGGING_ARG="-object filter-dump,id=hue,netdev=breh,file=e1000.pcap"

[ -e /dev/kvm ] && [ -r /dev/kvm ] && [ -w /dev/kvm ] && SEGUEBASE_VIRT_TECH_ARG="-enable-kvm"

[ -z "$SEGUEBASE_BOCHS_BIN" ] && SEGUEBASE_BOCHS_BIN="bochs"

# To support virtualization acceleration on mac
# we need to use 64-bit qemu
if [ "$(uname)" = "Darwin" ] && [ "$(uname -m)" = "x86_64" ]; then

    [ -z "$SEGUEBASE_QEMU_BIN" ] && SEGUEBASE_QEMU_BIN="qemu-system-x86_64"

    if $SEGUEBASE_QEMU_BIN --accel help | grep -q hvf; then
        SEGUEBASE_VIRT_TECH_ARG="--accel hvf"
    fi
fi

SCRIPT_DIR="$(dirname "${0}")"

# Prepend the toolchain qemu directory so we pick up QEMU from there
PATH="$SCRIPT_DIR/../Toolchain/Local/qemu/bin:$PATH"

# Also prepend the i686 toolchain directory because that's where most
# people will have their QEMU binaries if they built them before the
# directory was changed to Toolchain/Local/qemu.
PATH="$SCRIPT_DIR/../Toolchain/Local/i686/bin:$PATH"

SEGUEBASE_RUN="${SEGUEBASE_RUN:-$1}"

if [ -z "$SEGUEBASE_QEMU_BIN" ]; then
    if [ "$SEGUEBASE_ARCH" = "x86_64" ]; then
        SEGUEBASE_QEMU_BIN="qemu-system-x86_64"
    else
        SEGUEBASE_QEMU_BIN="qemu-system-i386"
    fi
fi

[ -z "$SEGUEBASE_KERNEL_CMDLINE" ] && SEGUEBASE_KERNEL_CMDLINE="hello"

[ -z "$SEGUEBASE_RAM_SIZE" ] && SEGUEBASE_RAM_SIZE=512M

[ -z "$SEGUEBASE_QEMU_CPU" ] && SEGUEBASE_QEMU_CPU="max"

[ -z "$SEGUEBASE_DISK_IMAGE" ] && {
    if [ "$SEGUEBASE_RUN" = qgrub ]; then
        SEGUEBASE_DISK_IMAGE="grub_disk_image"
    else
        SEGUEBASE_DISK_IMAGE="_disk_image"
    fi
}

if ! command -v "$SEGUEBASE_QEMU_BIN" >/dev/null 2>&1 ; then
    die "Please install QEMU version 5.0 or newer or use the Toolchain/BuildQemu.sh script."
fi

SEGUEBASE_QEMU_MIN_REQ_VERSION=5
installed_major_version=$("$SEGUEBASE_QEMU_BIN" -version | head -n 1 | sed -E 's/QEMU emulator version ([1-9][0-9]*|0).*/\1/')
if [ "$installed_major_version" -lt "$SEGUEBASE_QEMU_MIN_REQ_VERSION" ]; then
    echo "Required QEMU >= 5.0! Found $($SEGUEBASE_QEMU_BIN -version | head -n 1)"
    echo "Please install a newer version of QEMU or use the Toolchain/BuildQemu.sh script."
    die
fi

SEGUEBASE_SCREENS="${SEGUEBASE_SCREENS:-1}"
if (uname -a | grep -iq WSL) || (uname -a | grep -iq microsoft); then
    # QEMU for windows does not like gl=on, so detect if we are building in wsl, and if so, disable it
    SEGUEBASE_QEMU_DISPLAY_BACKEND="${SEGUEBASE_QEMU_DISPLAY_BACKEND:-sdl,gl=off}"
elif ("${SEGUEBASE_QEMU_BIN}" --display help | grep -iq sdl) && (ldconfig -p | grep -iq virglrenderer); then
    SEGUEBASE_QEMU_DISPLAY_BACKEND="${SEGUEBASE_QEMU_DISPLAY_BACKEND:-sdl,gl=on}"
elif "${SEGUEBASE_QEMU_BIN}" --display help | grep -iq cocoa; then
    # QEMU for OSX seems to only support cocoa
    SEGUEBASE_QEMU_DISPLAY_BACKEND="${SEGUEBASE_QEMU_DISPLAY_BACKEND:-cocoa,gl=off}"
else
    SEGUEBASE_QEMU_DISPLAY_BACKEND="${SEGUEBASE_QEMU_DISPLAY_BACKEND:-gtk,gl=off}"
fi

if [ "$SEGUEBASE_SCREENS" -gt 1 ]; then
    SEGUEBASE_QEMU_DISPLAY_DEVICE="virtio-vga,max_outputs=$SEGUEBASE_SCREENS "
    # QEMU appears to always relay absolute mouse coordinates relative to the screen that the mouse is
    # pointed to, without any way for us to know what screen it was. So, when dealing with multiple
    # displays force using relative coordinates only
    SEGUEBASE_KERNEL_CMDLINE="$SEGUEBASE_KERNEL_CMDLINE vmmouse=off"
else
    SEGUEBASE_QEMU_DISPLAY_DEVICE="VGA,vgamem_mb=64 "
fi

if [ -z "$SEGUEBASE_DISABLE_GDB_SOCKET" ]; then
  SEGUEBASE_EXTRA_QEMU_ARGS="$SEGUEBASE_EXTRA_QEMU_ARGS -s"
fi

[ -z "$SEGUEBASE_COMMON_QEMU_ARGS" ] && SEGUEBASE_COMMON_QEMU_ARGS="
$SEGUEBASE_EXTRA_QEMU_ARGS
-m $SEGUEBASE_RAM_SIZE
-cpu $SEGUEBASE_QEMU_CPU
-d guest_errors
-smp 2
-display $SEGUEBASE_QEMU_DISPLAY_BACKEND
-device $SEGUEBASE_QEMU_DISPLAY_DEVICE
-drive file=${SEGUEBASE_DISK_IMAGE},format=raw,index=0,media=disk
-usb
-device virtio-serial
-chardev stdio,id=stdout,mux=on
-device virtconsole,chardev=stdout
-device isa-debugcon,chardev=stdout
-device virtio-rng-pci
-soundhw pcspk
-device sb16
-device pci-bridge,chassis_nr=1,id=bridge1 -device e1000,bus=bridge1
-device i82801b11-bridge,bus=bridge1,id=bridge2 -device sdhci-pci,bus=bridge2
-device i82801b11-bridge,id=bridge3 -device sdhci-pci,bus=bridge3
-device ich9-ahci,bus=bridge3
"

[ -z "$SEGUEBASE_COMMON_QEMU_Q35_ARGS" ] && SEGUEBASE_COMMON_QEMU_Q35_ARGS="
$SEGUEBASE_EXTRA_QEMU_ARGS
-m $SEGUEBASE_RAM_SIZE
-cpu $SEGUEBASE_QEMU_CPU
-machine q35
-d guest_errors
-smp 2
-display $SEGUEBASE_QEMU_DISPLAY_BACKEND
-device $SEGUEBASE_QEMU_DISPLAY_DEVICE
-device secondary-vga
-device bochs-display
-device VGA,vgamem_mb=64
-device piix3-ide
-drive file=${SEGUEBASE_DISK_IMAGE},id=disk,if=none
-device ahci,id=ahci
-device ide-hd,bus=ahci.0,drive=disk,unit=0
-usb
-device virtio-serial
-chardev stdio,id=stdout,mux=on
-device virtconsole,chardev=stdout
-device isa-debugcon,chardev=stdout
-device virtio-rng-pci
-soundhw pcspk
-device sb16
"

export SDL_VIDEO_X11_DGAMOUSE=0

: "${SEGUEBASE_BUILD:=.}"
cd -P -- "$SEGUEBASE_BUILD" || die "Could not cd to \"$SEGUEBASE_BUILD\""

if [ "$SEGUEBASE_RUN" = "b" ]; then
    # Meta/run.sh b: bochs
    [ -z "$SEGUEBASE_BOCHSRC" ] && {
        # Make sure that SEGUEBASE_SOURCE_DIR is set and not empty
        [ -z "$SEGUEBASE_SOURCE_DIR" ] && die 'SEGUEBASE_SOURCE_DIR not set or empty'
        SEGUEBASE_BOCHSRC="$SEGUEBASE_SOURCE_DIR/Meta/bochsrc"
    }
    "$SEGUEBASE_BOCHS_BIN" -q -f "$SEGUEBASE_BOCHSRC"
elif [ "$SEGUEBASE_RUN" = "qn" ]; then
    # Meta/run.sh qn: qemu without network
    "$SEGUEBASE_QEMU_BIN" \
        $SEGUEBASE_COMMON_QEMU_ARGS \
        -device e1000 \
        -kernel Kernel/Kernel \
        -append "${SEGUEBASE_KERNEL_CMDLINE}"
elif [ "$SEGUEBASE_RUN" = "qtap" ]; then
    # Meta/run.sh qtap: qemu with tap
    sudo ip tuntap del dev tap0 mode tap || true
    sudo ip tuntap add dev tap0 mode tap user "$(id -u)"
    "$SEGUEBASE_QEMU_BIN" \
        $SEGUEBASE_COMMON_QEMU_ARGS \
        $SEGUEBASE_VIRT_TECH_ARG \
        $SEGUEBASE_PACKET_LOGGING_ARG \
        -netdev tap,ifname=tap0,id=br0 \
        -device e1000,netdev=br0 \
        -kernel Kernel/Kernel \
        -append "${SEGUEBASE_KERNEL_CMDLINE}"
    sudo ip tuntap del dev tap0 mode tap
elif [ "$SEGUEBASE_RUN" = "qgrub" ]; then
    # Meta/run.sh qgrub: qemu with grub
    "$SEGUEBASE_QEMU_BIN" \
        $SEGUEBASE_COMMON_QEMU_ARGS \
        $SEGUEBASE_VIRT_TECH_ARG \
        $SEGUEBASE_PACKET_LOGGING_ARG \
        -netdev user,id=breh,hostfwd=tcp:127.0.0.1:8888-10.0.2.15:8888,hostfwd=tcp:127.0.0.1:8823-10.0.2.15:23 \
        -device e1000,netdev=breh
elif [ "$SEGUEBASE_RUN" = "q35_cmd" ]; then
    # Meta/run.sh q35_cmd: qemu (q35 chipset) with SegueBaseOS with custom commandline
    shift
    SEGUEBASE_KERNEL_CMDLINE="$*"
    echo "Starting SegueBaseOS, Commandline: ${SEGUEBASE_KERNEL_CMDLINE}"
    "$SEGUEBASE_QEMU_BIN" \
        $SEGUEBASE_COMMON_QEMU_Q35_ARGS \
        $SEGUEBASE_VIRT_TECH_ARG \
        -netdev user,id=breh,hostfwd=tcp:127.0.0.1:8888-10.0.2.15:8888,hostfwd=tcp:127.0.0.1:8823-10.0.2.15:23 \
        -device e1000,netdev=breh \
        -kernel Kernel/Kernel \
        -append "${SEGUEBASE_KERNEL_CMDLINE}"
elif [ "$SEGUEBASE_RUN" = "qcmd" ]; then
    # Meta/run.sh qcmd: qemu with SegueBaseOS with custom commandline
    shift
    SEGUEBASE_KERNEL_CMDLINE="$*"
    echo "Starting SegueBaseOS, Commandline: ${SEGUEBASE_KERNEL_CMDLINE}"
    "$SEGUEBASE_QEMU_BIN" \
        $SEGUEBASE_COMMON_QEMU_ARGS \
        $SEGUEBASE_VIRT_TECH_ARG \
        -netdev user,id=breh,hostfwd=tcp:127.0.0.1:8888-10.0.2.15:8888,hostfwd=tcp:127.0.0.1:8823-10.0.2.15:23 \
        -device e1000,netdev=breh \
        -kernel Kernel/Kernel \
        -append "${SEGUEBASE_KERNEL_CMDLINE}"
elif [ "$SEGUEBASE_RUN" = "ci" ]; then
    # Meta/run.sh ci: qemu in text mode
    echo "Running QEMU in CI"
    "$SEGUEBASE_QEMU_BIN" \
        $SEGUEBASE_EXTRA_QEMU_ARGS \
        $SEGUEBASE_VIRT_TECH_ARG \
        -m $SEGUEBASE_RAM_SIZE \
        -cpu $SEGUEBASE_QEMU_CPU \
        -d guest_errors \
        -smp 2 \
        -drive file=${SEGUEBASE_DISK_IMAGE},format=raw,index=0,media=disk \
        -device ich9-ahci \
        -nographic \
        -display none \
        -debugcon file:debug.log \
        -kernel Kernel/Kernel \
        -append "${SEGUEBASE_KERNEL_CMDLINE}"
else
    # Meta/run.sh: qemu with user networking
    "$SEGUEBASE_QEMU_BIN" \
        $SEGUEBASE_COMMON_QEMU_ARGS \
        $SEGUEBASE_VIRT_TECH_ARG \
        $SEGUEBASE_PACKET_LOGGING_ARG \
        -netdev user,id=breh,hostfwd=tcp:127.0.0.1:8888-10.0.2.15:8888,hostfwd=tcp:127.0.0.1:8823-10.0.2.15:23,hostfwd=tcp:127.0.0.1:8000-10.0.2.15:8000,hostfwd=tcp:127.0.0.1:2222-10.0.2.15:22 \
        -device e1000,netdev=breh \
        -kernel Kernel/Kernel \
        -append "${SEGUEBASE_KERNEL_CMDLINE}"
fi
