# Patches for LLVM on SegueBaseOS

## `remove-wstring.patch`

Removes `wstring`s from the source code, as SegueBaseOS doesn't support them yet.

## `insert-ifdef-seguebase.patch`

This patch adds several defines in order to omit things not supported by SegueBaseOS.
