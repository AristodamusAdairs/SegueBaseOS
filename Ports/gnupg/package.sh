#!/usr/bin/env -S bash ../.port_include.sh
port=gnupg
version=2.3.0
useconfigure=true
configopts="--with-libgpg-error-prefix=${SEGUEBASE_INSTALL_ROOT}/usr/local \
  --with-libgcrypt-prefix=${SEGUEBASE_INSTALL_ROOT}/usr/local \
  --with-libassuan-prefix=${SEGUEBASE_INSTALL_ROOT}/usr/local \
  --with-ntbtls-prefix=${SEGUEBASE_INSTALL_ROOT}/usr/local \
  --with-npth-prefix=${SEGUEBASE_INSTALL_ROOT}/usr/local \
  --disable-dirmngr"
files="https://gnupg.org/ftp/gcrypt/gnupg/gnupg-${version}.tar.bz2 gnupg-${version}.tar.bz2 84c1ef39e8621cfb70f31463a5d1d8edeab44332bc1e0e1af9b78b6f9ed05bb4"
auth_type=sha256
depends="libiconv libgpg-error libgcrypt libksba libassuan npth ntbtls"

pre_configure() {
    export GPGRT_CONFIG="${SEGUEBASE_INSTALL_ROOT}/usr/local/bin/gpgrt-config"
    export CFLAGS="-L${SEGUEBASE_INSTALL_ROOT}/usr/local/include"
    export LDFLAGS="-L${SEGUEBASE_INSTALL_ROOT}/usr/local/lib -lm -liconv -ldl"
}

configure() {
    run ./configure --host="${SEGUEBASE_ARCH}-pc-seguebase" --build="$($workdir/build-aux/config.guess)" $configopts
}
