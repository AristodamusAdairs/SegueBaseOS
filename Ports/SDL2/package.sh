#!/usr/bin/env -S bash ../.port_include.sh
port=SDL2
version=git
workdir=SDL-main-seguebase
useconfigure=true
files="https://github.com/SegueBaseOS/SDL/archive/main-seguebase.tar.gz SDL2-git.tar.gz"
configopts="-DCMAKE_TOOLCHAIN_FILE=${SEGUEBASE_SOURCE_DIR}/Toolchain/CMake/CMakeToolchain.txt -DPULSEAUDIO=OFF -DJACK=OFF"

configure() {
    run cmake $configopts
}

install() {
    run make install
}
