#!/usr/bin/env bash
export SEGUEBASE_SOURCE_DIR="$(realpath "${SCRIPT}/../")"
export SEGUEBASE_BUILD_DIR="${SEGUEBASE_SOURCE_DIR}/Build/${SEGUEBASE_ARCH}"
export CC="${SEGUEBASE_ARCH}-pc-seguebase-gcc"
export CXX="${SEGUEBASE_ARCH}-pc-seguebase-g++"
export AR="${SEGUEBASE_ARCH}-pc-seguebase-ar"
export RANLIB="${SEGUEBASE_ARCH}-pc-seguebase-ranlib"
export PATH="${SEGUEBASE_SOURCE_DIR}/Toolchain/Local/${SEGUEBASE_ARCH}/bin:${HOST_PATH}"
export PKG_CONFIG_DIR=""
export PKG_CONFIG_SYSROOT_DIR="${SEGUEBASE_BUILD_DIR}/Root"
export PKG_CONFIG_LIBDIR="${PKG_CONFIG_SYSROOT_DIR}/usr/lib/pkgconfig/:${PKG_CONFIG_SYSROOT_DIR}/usr/local/lib/pkgconfig"

# To be deprecated soon.
export SEGUEBASE_ROOT="$(realpath "${SCRIPT}/../")"

enable_ccache

DESTDIR="${SEGUEBASE_BUILD_DIR}/Root"
export SEGUEBASE_INSTALL_ROOT="$DESTDIR"
